# IPFS systemD Service (Red Hat)
# Daemon for a Daemon
Run IPFS daemon as a systemD service and restart whenever it inevitably fails
## TODOS
- write setup script
- complete service file
- proper logging
- integrated SELinux policy
- test restart
## Prereqs
- Install [IPFS](https://ipfs.io/docs/install/)
## Install
Run setup:
```bash
git clone https://Akcipitro@bitbucket.org/Akcipitro/ipfs-service-red-hat-systemd.git
cd ipfs-service-red-hat-systemd/
./setup
```
Start service:
```bash
sudo systemctl start ipfsd
```
Check service:
```bash
sudo systemctl status ipfsd
```
If you see something like this:
<picture of SELinux policy prompt>
?Follow the suggested commands
?Install <this> policy files
